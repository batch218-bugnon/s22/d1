/*


// Functions Review


            //1    2     3   (4) - size of array  
            //0    1     2    3     4- indeces
let vowels = ["a", "e", "i", "o"]; 

// function - function keyword
// addVowel - function name 
    //For best practice, it is better to name our function in verb, to distinguish easily it's purpose
    //The function name should indicate the purpose of the function block 

                //parameters - is a variable where we store our values passed by argument
function addVowel(newVowel){ // newVowel = "u";

    // newVowel variable through parameter is accessible inside the function.
    vowels[vowels.length] = newVowel;
    console.log(vowels);

    //arrayName[index]
    //inside the square bracket must be an index / index number.
}
        
    //argument - argument/s is what we pass to parameter
addVowel("u"); //invocation //call 



function updateVowel(indexNum, newVowel){
    vowels[indexNum] = newVowel;
    console.log(vowels);
}
        //indexNum   //newVowel
updateVowel(1, "E");


let basketBallTeam = ["Rafael", "Enrico", "Ren", "Leonardo", "Jan Jonell"];

function addPlayer(player){
    basketBallTeam[basketBallTeam.length] = player; 
    console.log(basketBallTeam);
}

addPlayer("Joseph");


*/





//  Array Methods 
/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/


// Mutator Methods
// functions / methods that mutate or change an array 
// These manipulates the original array performing task such as adding and removing elements.


console.log("Mutator Methods");
console.log("---------------");
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
    Adds an element in the end of an array AND returns the new array's length 
    Syntax: 
        arrayName.push(newElement);
*/

console.log("=>Push()");
console.log("fruits array: ");
console.log(fruits);


// fruits[fruits.length] = "Mango";
let fruitsLength = fruits.push("Mango");
console.log("Size/length of fruits array: " + fruits.length);
console.log("Mutated array from push('Mango')");
console.log(fruits);


fruits.push("Avocado","Guava");
// console.log(fruits.push("Avocado","Guava")); // returns the new array's length
console.log("Mutated array from push('Avocado','Guava'): ");
console.log(fruits);


/*
function addMultipleFruits(fruit1, fruit2, fruit3){
    fruits.push(fruit1,fruit2,fruit3);
    console.log(fruits);
}
addMultipleFruits("Durian", "Atis", "Melon");

*/


// pop()
/*
    Removes the last item in an array AND returns the removed element
    
*/
console.log("---------------");
console.log("=>pop()");

let removedFruit = fruits.pop();
console.log(removedFruit); // returns the removed element
console.log("Mutated Array from pop method: ");
console.log(fruits);


// unshift()
/*
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log("---------------");
console.log("=>unshift()");

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method 'Lime', 'Banana') ");
console.log(fruits);


// shift()
/*
 Removes an element at the beginning of an array AND returns the removes statement
*/

console.log("---------------");
console.log("=>shift()");
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);




// splice();
/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log("---------------");
console.log("=>splice()");

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated Array from splice method: ");
console.log(fruits);


// we could also use splice to remove an element/s
// to do that our sysntax should be:
/*
    Syntax: 
        arrayName.splica(startingIndex, deleeteCount);
*/

/*

fruits.splice(2,4);
console.log(fruits)
*/


// sort();

/*
    -Rearranges the array elements in alphanumeric order
    -Syntax:
        - arrayName.sort();
*/

console.log("---------------");
console.log("=>sort()");
fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);




// reverse 
/*
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/


console.log("---------------");
console.log("=>reverse()");
fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);




// Non-mutator 
/*
    Non-mutator methods are functions that do not modify of change an array after they're created.


*/


console.log("-----------");
console.log("-----------");
console.log("==Non-mutator Methods==");
console.log("-----------");

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

console.log("=>indexOf()");


let firstIndex = countries.indexOf("PH");
console.log("Result of the indexOf('PH') " + firstIndex);


                                // "PH" element to add
                                // starting index where to search 
/* let firstIndex = countries.indexOf("PH", 2); // with a specified index to start
console.log("Result of the indexOf('PH', 2) " + firstIndex); */ 


let invalidCountry = countries.indexOf("BR");
console.log("Result of the indexOf('BR'): " + invalidCountry); // -1 



// lastIndexOf()
// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/

    console.log("-----------");
    console.log("=>lastIndexOf()");
    let lastIndex = countries.lastIndexOf("PH");
    console.log("Result of lastIndexOf('PH'): " +lastIndex);


    let lastIndexStart = countries.lastIndexOf("PH", 4);
    console.log("Result of lastIndexOf('PH', 4): " + lastIndexStart);



// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/
console.log("-----------");
    console.log("=>slice()");

    console.log("Original countries array:");
    console.log(countries);

// Slicing off elements from specified index to the last element 

let sliceArrayA = countries.slice(2);
// "US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"
    console.log("Result from slice(2):");
    console.log(sliceArrayA);



// Slicing off element from specified index to another index. But the specified last index is not included in the return.
    let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
    console.log("Result from slice(2, 5):");
    console.log(sliceArrayB);


// slicing off elements starting from the last element of an array.
   //-8   //-7  //-6  //-5   //-4 //-3  //-2  //1
// "US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"

    let sliceArrayC = countries.slice(-3);
    console.log("Result from slice method:");
    console.log(sliceArrayC);


// toString 

/*

    Returns an array as a string, separated by commas/
    Syntax
        arrayName.toString();
*/


    console.log("-----------");
    console.log("=>toString()");

    // ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
    let stringArray = countries.toString();
    console.log("Result from toString method: ");
    console.log(stringArray);
    console.log(stringArray[0]); // U
    console.log(typeof stringArray); //to check if the array is converted to string.



// Nov 7, 2022 Continuation of discussion 

// concat 
/*
    - combines two arrays AND returns the combined result.
    - Syntax:
        arrayA.concat(arrayB);
        arrayA.concat(element);
*/

    console.log("-----------");
    console.log("=>concat()"); // shortcut for concatenation

    let tasksArrayA = ["drink html","eat javascript"];
    let tasksArrayB = ["inhale css", "breathe sass"];
    let tasksArrayC = ["get git", "be node"];

    console.log("tasksArrayA: ");
    console.log(tasksArrayA);
    console.log("tasksArrayB: ");
    console.log(tasksArrayB);
    console.log("tasksArrayC: ");
    console.log(tasksArrayC);

    let tasks = tasksArrayA.concat(tasksArrayB);

    console.log("Result from concatinating tasksArrayA and tasksArrayB: ");
    console.log(tasks);


    // Combining multiple arrays 
    console.log("Result from concatinating all tasks array: ");
    let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
    console.log(allTasks);

    // Combining array with elements
    let combinedTasks = tasksArrayA.concat("smell express", "throw react");
    console.log("Result from concatinating tasksArrayA with other elements: ");
    console.log(combinedTasks);


    // join()
    /*
    - Returns an array as a string separated by specified separator string.
    - Syntax"
        arrayName.join("separatorString");
*/

     console.log("-----------");
     console.log("=>join()")
     let users = ["Josephine", "Enola", "John Snow", "Gambit"];
     console.log(users);
     console.log(users.join());
     console.log(users.join(""));
     console.log(users.join(" "));
     console.log(users.join(" - "));


     // Iteration Methods 

     /*
    - Similar to a for loop that iterates on each array element.
    -  For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to received the current item being iterated or loop over.
    - Variable names for arrays are written in plural form of the data stored.
    - It's common practice to use the singular form of the array content for parameter names used in array loops.
    - forEach() does not return anything.
    - Syntax:
        arrayName.forEach(function(indivElement){
            //statement/code block.
        })
    */


    console.log("-----------");
    console.log("Output using for loop: ");
        //0              //1           //2           //3            //4         //5
   // ["drink html","eat javascript", "inhale css", "breathe sass", "get git", "be node"];
        // initialization   //condition        //change of value
    for (let index = 0; index < allTasks.length; index++){
        console.log(allTasks[index]);
    }
    // the codes inside the loop will only execute if the condition returned value is true

    // after executing code inside the loop, the program will evaluate the condition again


    //index = 0, index < 5    // true
        // console.log(allTasks[index]); => "drink html"
        // index ++ or index == index + 1 => 1
  
    // index = 1, index < allTask.length  // true
        // console.log(allTasks[index]); => "eat javascript"
        // index ++ => 2
  
    // index = 2, index < allTask.length  // true
        // console.log(allTasks[index]); => "inhale css"
        // index ++ => 3

    // index = 3, index < allTasks.length // true 
        // console.log(allTasks[index]); => "breath mongoDB"
        // index ++ => 4

    // index = 4, index < allTasks.length // true 
        // console.log(allTasks[index]); => "get git"
        // index ++ => 5

    // index = 5, index < allTasks.length // true 
        // console.log(allTasks[index]); => "be node"
        // index ++ => 6

    // index = 6, index < allTasks.length // false 
        // exit the loop


    // LOOP PATTERN 
    // declare/initialize value 
        // evaluate condition 
        // execute if returned value is true, stop if false 
        // change of value, reassign, back to evaluation condition 



    console.log("-----------");
    console.log("Output using forEach: ");
    let filteredTasks = []; //store in this variable all task names with characters greater than 10.

                // "task" anonymous function "parameter" represents each element of the array to be iterated.
    allTasks.forEach(function(task){
        console.log(task);

        // If the element/string's length is greater than 10 characters

        if(task.length > 10){
            filteredTasks.push(task);
        }

    });

    console.log("Result of filtered tasks: ");
    console.log(filteredTasks);



    // map()
    /*
    - Iterates on each element and returns new array with different values depending on the result of the function's operation.
    - This is useful for performing tasks where mutating/changing the elements required.

    -Syntax
        let/const resultArray = arrayName.map(function(indivElement));
    */

    console.log("-----------");
    console.log("=>map()");
    let numbers = [1, 2, 3, 4, 5];

    // return squared values of each element.
    let numbersMap = numbers.map(function(number){
        return number * number
    });

    console.log("Original Array: ");
    console.log(numbers);

    console.log("Result of the map method: ");
    console.log(numbersMap);


    // map() best to display per element  vs forEach() per array 
    console.log("-----------");
    console.log("map() vs forEach");
    let numberForEach = numbers.map(function(number){
        // console.log(number*number);
        return number * number
    });

    console.log(numberForEach);



    // every()
/*
    - checks if ALL elements in an array meet the given condition. 
    - This is useful for validation data stored in arrays especially when dealing with large amounts of data.
    - Returns a true value if all elements meet the condition and false if otherwise.
    -Syntax:
        let/const = resutlArray = arrayName.every(function(indivual element){
            return expression/condition;
        })
*/

    console.log("-----------");
    console.log("=>every()");
    numbers = [ 5, 7, 9, 1, 5];
    // - checks if ALL elements in an array meet the given condition. 
    let allValid = numbers.every(function(number){
    return (number < 3);
    });

    console.log("result of every method: ");
    console.log(allValid);



    // some()
/*
    - Check if at least one element in the array meets the given condition.
    - Returns a true if  at least one elements meets the conditon and false otherwise.
    -Syntax:
        let/const resultArray = arrayName.some(function(individElement){
            return expression/condition;
        })
*/

    console.log("-----------");
    console.log("=>some()");
    // - Check if at least one element in the array meets the given condition.
    let someValid = numbers.some(function(number){
    return (number < 2);
    })
    console.log("Result of some method: ");
    console.log(someValid);

    // Combining the returned results from the every/some metho may be used in other statements to perform consecutive result.
    if(someValid){
    console.log("Some numbers in the array are greater than 2.");
}
    



    // filter()
/*
    - Return a new array that contaions elements which meets the given condition.
    - Return an empty array if no elements were found.
    - Useful for filtering array elements with given condition and shortens the syntax.
    - Syntax:
        let/const resultArray = arrayName.filter(function(indivElement){
            return expression/condition:
        })
*/

    console.log("-----------");
    console.log("=>filter()");
    numbers = [1, 2, 3, 4, 5];
            // filterValid - variable where store the result
            // numers - array name where we base 
            // filter - method keyword
            // function and parameter
    let filterValid = numbers.filter(function(number){
        return (number < 3);
    });

    console.log("Result of filter method:");
    console.log(filterValid);



    // No elements found
    let nothingFound = numbers.filter(function(number){
        return number == 0;
    })
    console.log("Result of filter method: ");
    console.log(nothingFound);


    // shorten the syntax
    filteredTasks = allTasks.filter(function(task){
        return (task.length > 10);
    })



    // some equal to or 
    // every equal to and 
    // filter return the element that satisfies the condition




    // includes()
    /*
        - checks if the argument passed can be found in the array.
        - it returns a boolean which can be saved in a variable
        - Syntax:
            let/const variableName = arrayName.include(<argumentToFind>);
    */

    // efficient for searching inside an array


    console.log("-----------");
    console.log("=>includes()");
    let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

    let productFound = products.includes("Mouse");
    console.log("Result of includes method: ");
    console.log(productFound); //returns true


    let productNotFound = products.includes("Headset");
    console.log("Result of includes method:");
    console.log(productNotFound);



    // Method chaining
    // The result of the inner method is used in the outer method until all "chained" methods have been resolved. 

    console.log("-----------");
    console.log("[Method chaining]");
    //products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

    //filter products array, display only products with letter a
    let filteredProducts = products.filter(function(product){ // filter returns element
                //keyboard
        return product.toLowerCase().includes('a'); // returns True or false 
    })    

    console.log("Result of the chained method: ")
    console.log(filteredProducts);




// reduce()
/*
    - Evaluates elements from left and right and returns/reduces the array into a single value.
    - Syntax:
        let/const resultVariable = arrayName.reduce(function(accumulator, currentValue){
            return expression/operation
        });
            - accumulator parameter stores the result for every loop.
            - currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop. 
*/    


    console.log("-----------");
    console.log("=>reduce");
    let i = 0;

    numbers = [1, 2, 3, 4, 5];

    let reducedArray = numbers.reduce(function(acc, cur){
        // console.warn("current iteration " + ++i);
        console.log("accumulator: " +acc); //1  //3  //6 //10
        console.log("current value:" + cur); //2  //3  //4 
        //5

        // The operation or expression to reduce the array into a single value.
        return acc + cur; //3  //6  //10 //15
    });


    console.log("Result of reduce method: " + reducedArray);




















